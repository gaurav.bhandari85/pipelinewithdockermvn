FROM openjdk:8-jdk

LABEL Author="Gaurav Bhandari"
LABEL Email="gaurav.bhandari85@gmail.com"
LABEL phone="Cannot Share"
LABEL location="REAL GITLAB"

RUN apt-get update
RUN apt-get install -y maven

COPY test/pom.xml  /usr/local/service/pom.xml
COPY test/src /usr/local/service/src

WORKDIR /usr/local/service

RUN mvn package

CMD ["java", "-cp", "target/test-1.0-SNAPSHOT.jar", "com.gb.App"]

